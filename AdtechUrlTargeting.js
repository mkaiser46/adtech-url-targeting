/**
 * Copyright 2015 AOL Advertising.
 * 
 * URL Targeting Script
 * 
 * version: 1.2
 *
 * Author: Magnus Kaiser
 */

 window.AdtechUrlTargeting = {
 	//configurations
 	errorReporting: false,
 	logSentParameters: false,
 	useAlerts: false,
 	urlSeperator: "/",
 	transmitGetKeyAndValue: true,
 	keyName: 'url',
 	combine: true,
 	useBlacklist: false,
 	result: {},
 	blacklist: [],	
 	transmitProtocol: true,
 	transmitDomain: true,
	baseUrl: window.location.pathname + window.location.search,

	//runtime variables - DO NOT CHANGE
	getControl: 0,
	config: {},

 	setup: function() {

 		//check for custom settings
 		if (Object.keys(this.config).length > 0) {
 			for (var key in this.config) {
				if (AdtechUrlTargeting.hasOwnProperty(key)) {
					//set custom config
					AdtechUrlTargeting[key] = AdtechUrlTargeting.config[key];
				};
 			};
 		};

 		error = this.checkConfig();
 		if (error > 0) {
 			//error with the configuration -> exiting
 			if (this.errorReporting && this.useAlerts) {
 				alert("ADTECH URL Targeting setup is wrong! NO values have been added! Error code: "+error);
 			} else if (this.errorReporting){
 				console.log("ADTECH URL Targeting setup is wrong! NO values have been added! Error code: "+error);
 			}
 		} else {
 			this.getParameters();
 			if (this.logSentParameters) {
 				console.log("Sent Parameters:");
 				console.log(this.result);
 			};
 			
 		}
 	},

 	checkConfig: function() {
 		if (this.errorReporting) {
 			if (this.useAlerts) {
 				alert("Warning: Error Reporing is enabled. This should not be the case for production enviroments");
 			} else {
 				console.log("Warning: Error Reporing is enabled. This should not be the case for production enviroments");
 			}
 		};

 		if (this.logSentParameters) {
 			if (this.useAlerts) {
 				alert("Warning: Sent parameter logging is enabled. This should not be the case for production enviroments");
 			} else {
 				console.log("Warning: Sent parameter logging is enabled. This should not be the case for production enviroments");
 			}
 		};

 		if (this.urlSeperator == '') {
 			return 1;
 		};

 		if (this.transmitGetKeyAndValue && this.urlSeperator != '&' && !this.combine) {
 			return 2;
 		};

 		if (this.urlSeperator == '&' && this.transmitGetKeyAndValue && this.keyName != '') {
 			if (this.useAlerts) {
 				alert("Warning: keyName is set but the setup doesn't allow this option");
 			} else {
 				console.log("Warning: keyName is set but the setup doesn't allow this option");
 			}
 		};

 		if (this.useBlacklist && Object.keys(this.blacklist).length == 0) {
 			return 3;
 		};

 		if (this.baseUrl == '') {
 			return 4;
 		};

 		if (this.combine) {
 			if (!this.transmitGetKeyAndValue && this.keyName == "") {
 				return 5;
 			};

 			if (this.urlSeperator == "&") {
 				return 6;
 			};
 		};

 		return 0;
 		
 	},

 	getParameters: function() {
 		if (this.transmitDomain) {
  			AdtechUrlTargeting.result["domain"] = window.location.host;
 		};

 		if (this.transmitProtocol) {
			AdtechUrlTargeting.result["protocol"] = window.location.protocol.substring(0, window.location.protocol.length - 1);
 		};


	 	if (this.urlSeperator == "&") {
	 		this.fetchGetParameters();
	 	} else {
 			//URL uses mod_rewrite
 			_url = this.baseUrl.substring(1);
	 		_values = _url.split(this.urlSeperator);
 			_values.forEach(function(data, index) {

 				if (AdtechUrlTargeting.combine) {				
 					//check if any element has GET layout
 					if (data.indexOf("?") != -1) {
						//split file and GET data
						tmp = data.split("?");

						//add file as key value
						if (Object.keys(AdtechUrlTargeting.result).length == 0) {
		 					AdtechUrlTargeting.result[AdtechUrlTargeting.keyName] = tmp[0];
		 				} else {
		 					AdtechUrlTargeting.result[AdtechUrlTargeting.keyName] += ":" + tmp[0];
		 				}

						//get GET parameters from query string
						AdtechUrlTargeting.fetchGetParameters(tmp[1]);		
					} else {
						if (AdtechUrlTargeting.result[AdtechUrlTargeting.keyName] === undefined) {
		 					AdtechUrlTargeting.result[AdtechUrlTargeting.keyName] = data;
		 				} else {
		 					AdtechUrlTargeting.result[AdtechUrlTargeting.keyName] += ":" + data;
		 				}
					}
 				} else {
 					if (AdtechUrlTargeting.result[AdtechUrlTargeting.keyName] === undefined) {
	 					AdtechUrlTargeting.result[AdtechUrlTargeting.keyName] = data;
	 				} else {
	 					AdtechUrlTargeting.result[AdtechUrlTargeting.keyName] += ":" + data;
	 				}
 				}
			});
	 	};
 	},

 	fetchGetParameters: function(specificValue) {
 		//URL uses GET layout
 		if (specificValue === undefined) {
			_url = this.baseUrl.substring(1); //this removes the "?" at the beginning
 			_values = _url.split("&");
 		} else {
 			//post-processign of values
 			_url = specificValue;
 			_values = _url.split("&");
 		}
 		

 		if (this.transmitGetKeyAndValue == true) {
 			//get GET parameters and values
 			_values.forEach(function(data, index) {
 				tmp = data.split("=");
 				if (AdtechUrlTargeting.blacklist.indexOf(tmp[0]) == -1) {
 					//key is not blacklisted
 					AdtechUrlTargeting.result[tmp[0]] = tmp[1];
 				};
 			});
 		} else {
 			//only get values
 			_values.forEach(function(data, index) {
 				tmp = data.split("=");
 				if (AdtechUrlTargeting.result[AdtechUrlTargeting.keyName] === undefined) {
 					AdtechUrlTargeting.result[AdtechUrlTargeting.keyName] = tmp[1];
 				} else {
 					AdtechUrlTargeting.result[AdtechUrlTargeting.keyName] += ":" + tmp[1];
 				}
 			});
 		};

 		//reset getControl in case of another loop
 		this.getControl = 0;
 	}
}