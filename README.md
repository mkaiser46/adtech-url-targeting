# README #

The AdtechUrlTargeting Object is a light weight and easy to use script that allows ad requests to use parameters from the URL to be passed as key values.

Example for GET parameters only: http://iqtestsites.adtech.de/magnus/AdtechUrlTargeting/index.html?key1=value1&key2=value2&key3=value3&key4=value4

Example for combines seperators: http://iqtestsites.adtech.de/magnus/AdtechUrlTargeting/combined/index.html?key1=value1&key2=value2&key3=value3&key4=value4

CDN link: http://aka-cdn.adtech.de/dt/common/AdtechUrlTargeting.js or https://aka-cdn.adtech.de/dt/common/AdtechUrlTargeting.js

### What is this repository for? ###

* Web Developer Information
* Requirements & Limitations
* Versions
* Roadmap

### Web Developer Information ###

Integrating the AdtechUrlTargeting object is very easy and just requries a few simple steps. As a reference, please see the link above in the general notes.

1. Add the adtechUrlTargeting.js as a reference in the head of every web page.
2. Configure the settings of the script (see Configuration)
3. Add __AdtechUrlTargeting.setup()__ on the website before the ADTECH DAC configurations. You may want to use a blacklist (see Attributes: __backlist__) which requires to set the AdtechUrlTargeting.blacklist = ["element1","element2"] attribute before calling the __AdtechUrlTargeting.setup()__ method

#### Configuration ####

Since version 1.2 default settings will be used unless specified otherwise on the webiste. The default settings are as below:

	errorReporting: false,
	logSentParameters: false,
	useAlerts: false,
	urlSeperator: "/",
	transmitGetKeyAndValue: true,
	keyName: 'url',
	combine: true,
	useBlacklist: false,
	result: {},
	blacklist: [],	
	transmitProtocol: true,
	transmitDomain: true,
	baseUrl: window.location.pathname + window.location.search,

Custom configurations must be set before __AdtechUrlTargeting.setup()__. All above options can be changed by simply adding them to the __AdtechUrlTargeting.config__ object. Non-changed values will automatically use the default settings.

	Example:

	AdtechUrlTargeting.config = {
		useAlerts: true,
		urlSeperator: "&",
		errorReporting: true,
		combine: false,
		baseUrl: window.location.search,
		useBlacklist: true,
		blacklist: ["key1","key2"],
	}

##### Attributes #####

__errorReporting__
When enabled, error messages will be displayed via _alert()_ or _console.log()_ (see __useAlerts__) to inform about incorrect configurations.

_Note:_ This should not be enabled in a production enviroment but for testing and debugging only.

__useAlerts__
If set to _true_ the error message will be displayed via _alert()_ instead of logged in the developer console.


__logSentParameters__
If set to _true_ all key values will be logged in the console in form of a JavaSript objects. This feature is for debugging only and should be disabled in production enviroment.

__urlSeperator__
Should the web server allow mod_reqrite and rules are setup, a specific seperator (usually slash "/") can be selected what will break up the URL in the values that will be transmitted as key value.

	Example:
		URL: http://www.myWebiste.com/section/topic/articleId
		urlSeperator: /
		transmitted values:	- section
							- topic
							- sectionId

_Note:_ Should the server not allow mod_rewrite, the URL will contain "&" signs as sepeartors. These should be then setup as the __urlSeperator__ to ensure correct values are getting parsed from the URL into the tags.

__transmitGetKeyAndValue__
This URL form is the standard GET option displaying keys and values seperated by "&" within the URL. This "&" sign needs to be setup as the __urlSeperator__ but it can be selected as which key it should be transmitted. Is __transmitGetKeyAndValue__ set to _true_ the GET key will be used as key in the tags and the GET value as the key value in the ADTECH tags.

_Note:_ Since v1.1 it is possible to allow this setting when the URL contains a custom delimiter and GET parameters. This setting can alos be used when using sub-folders which are delimited in the URL by "/".

__keyName__
This value will only be used when the URL layout uses mod_rewrite and a specific seperator that only shows the value but not assosiated key for it. If the URL type is GET like, this attribute will be ignored, unless __combine__ is _true_.

__combine__
This feature allow to combine custom delimniters and GET paremeters into key values for an ad request. If enabled, the __keyName__ field must be set or __transmitGetKeyAndValue__ must be _true_.

_Note:_ If combined is enabled, the __baseURL__ must be set to the below to enable the correct key value transmission.

	baseUrl: window.location.pathname + window.location.search,


__useBlacklist__
This attribute allows to blacklist certain GET key values to be transmitted i.e. session tokens or other internal temporary data that cannot be used for trageting due do it's random nature and short live time.
The keys that should be ignored must be provided as array (even when there is only one value).

Within the website, this can be easily setup by setting this property directly _before_ the __AdtechUrlTargeting.setup()__ call. The attribute _requires_ to be set as a array, even when only one element is given.
	
	Example:
	URL: http://www.myWebiste.com/inex.php?key1=value2&key2=value2
	blacklist: ["key1"]
	transmitted values:	- key2 = value2

_Note:_ This feature is only supported for GET layout URLs, unless __combine__ is enabled too.
_Note:_ Since v1.2 this can also be set in the __AdtechUrlTargeting.config__ object.

__result__
After __AdtechUrlTargeting.setup()__ has been called, the key value for the targeting can be received via this property. This will return a JSON object (as required by the DAC libary) that can then be added to a webiste, page or placement configuration.

	Example:
		AdtechUrlTargeting.blacklist = ["key1","key4"]; //set key1 an key4 as blacklisted key
		AdtechUrlTargeting.setup(); //process the URL

		ADTECH.config.page = {
			protocol: 'http',
			network: '25.7',
			kv: AdtechUrlTargeting.result, //add the key values to the page configuration
		}

__baseUrl__
The part of the URL that should be processed must be set here. Below the recommended setting, depending on the URl layout. It might be possible to use a different source, but this depends mainly on the structure of the URl.

	mod_rewrite format: window.location.pathname
	GET format: window.location.search
	combined: window.location.pathname + window.location.search

__transmitProtocol__
Set this to true in order to get the protocol (http, https, file) of the current webiste in the key value _protocol_.

__transmitDomain__
Set this to true in order to get the current domain (including sub-domain up to the tld) of the current webiste in the key value _domain_.

#### Error Codes & Warning Messages ####

- 1 __urlSeperator__ isn't defined
- 2 __transmitGetKeyAndValue__ is enabled but the seperator doesn't support it
- 3 __useBlacklist__ is enabled but no entries are given
- 4 __baseUrl__ is empty
- 5 __combine__ ien enabled but __transmitGetKeyAndValue__ is _true_ nor __keyName__ is set
- 6 __combine__ ien enabled but the __urlSelector__ is "&"

The following events cause a warning message:
__errorReporting__ is enabled
__urlSeperator__ is '&' and the __transmitGetKeyAndValue__ flag is set to _true_ but the __keyName__ is set. This is becase the __keyName__ attribute doesn't get used in this kind of setup.

### Requirements & Limitations ###

* ADTECH DAC tag is required

__Limitations:__
- when __keyName__ is the same as one GET key, only the GET parameter will be transmitted

### Version ###

#### v1.0 ####
- first version giving basic functionality

#### v1.1 ####
- added __combined__ attribute to allow delimiters + GET parameter checks
- added console logging for transmitted keys and values

#### v1.2 ####
- added custom configuration options
- now available via CDN: http://aka-cdn.adtech.de/dt/common/AdtechUrlTargeting.js or https://aka-cdn.adtech.de/dt/common/AdtechUrlTargeting.js

### Roadmap ###

* nothing so far; suggestion please to magnus.kaiser@adtech.com